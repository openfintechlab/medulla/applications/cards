/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";
import CardsController              from "../mapping/CardsController";
import logger from "../utils/Logger";

const router: any = express.Router();


router.get('/card/:id', (req:any,res:any) =>{
    res.set("Content-Type","application/json; charset=utf-8");
    if(req.query.type === undefined || (req.query.type !== 'cif' && req.query.type !== 'cc')){
        res.status(404);   
        res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9404","Invalid Request!"));        
    }else{
        let cardsController:CardsController = new CardsController();
        if(req.query.type === 'cif') {
            cardsController.cif = req.params.id;
        }else if(req.query.type === 'cc'){
            cardsController.pan = req.params.id;            
        }
        cardsController.getCardsDetails().then((response:String) => {
            logger.debug(response);
            res.status(200)
            res.send(response);
        }).catch((error:any) => {
            logger.debug(error);
            res.status(500)
            res.send(error);        
        });        
    }    
});


/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);    
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;