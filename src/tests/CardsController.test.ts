import CardsController from "../mapping/CardsController";

describe("Test casees for testing Cards Controller", ()=>{
    test('Instantiate the controller', () => {
        let cardsController:CardsController = new CardsController();
        expect(cardsController).toBeDefined();
    });

    test('Setting CIF properties', () => {
        let cardsController:CardsController = new CardsController();
        cardsController.cif = "5213444";
        cardsController.getCardsDetails().then((response:String) => {
            let jsonResp:any = JSON.parse(response.toString());
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("0000");
            expect(jsonResp.cards.length > 0).toBeTruthy();
        })
    });

    test('Setting PAN properties', () => {
        let cardsController:CardsController = new CardsController();
        cardsController.pan = "4289484745510834";
        cardsController.getCardsDetails().then((response:String) => {
            let jsonResp:any = JSON.parse(response.toString());
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("0000");
            expect(jsonResp.cards.length > 0).toBeTruthy();
        });
    });

    test('Checking Invalid input condition 9500', () => {
        let cardsController:CardsController = new CardsController();        
        cardsController.getCardsDetails().then((response:String) => {
            fail(response);
        }).catch((error: String) => {
            let jsonResp:any = JSON.parse(error.toString());
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("9500");            
        });
    });

    xtest('Checking record not found condition', () => {
        let cardsController:CardsController = new CardsController();        
        cardsController.pan = "4289484745510899";
        cardsController.getCardsDetails().then((response:String) => {
            let jsonResp:any = JSON.parse(response.toString());            
            expect(jsonResp).toBeDefined();
            expect(jsonResp.metadata.status).toBe("8404");            
        });

    });
});