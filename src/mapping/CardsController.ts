import {PostRespBusinessObjects}        from "./bussObj/Response";
import CardsResponse,{CardBO}                   from "./bussObj/Cards";
import cardJSON                         from "./simulator/cardssim.json";

export default class CardsController{
    private _pan!: string;
    private _cif!: string;

    set pan(pan: string){
        this._pan = pan;
    }

    set cif(cif: string){
        this._cif = cif;
    }

    public getCardsDetails(): Promise<String>{
        return new Promise<String>((resolve: any, reject: any) => {            
            let cards:Array<CardBO> = new Array<CardBO>();
            let cardResp:CardBO;
            let objCountr:number = 0;
            if(this._pan === undefined && this._cif === undefined){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9500","Invalid Request"));
            }
            for(let index in cardJSON.cards){
                if(cardJSON.cards[index].pan === this._pan || 
                    cardJSON.cards[index].cif === this._cif){                                        
                    cardResp = new CardBO(cardJSON.cards[index].pan,                
                        cardJSON.cards[index].cif,                
                        cardJSON.cards[index].accountNumber,      
                        cardJSON.cards[index].currency,           
                        cardJSON.cards[index].type,               
                        cardJSON.cards[index].isActivated,        
                        cardJSON.cards[index].isSupp,             
                        cardJSON.cards[index].availBal,           
                        cardJSON.cards[index].availBalDrCrInd,    
                        cardJSON.cards[index].outstandingBalance, 
                        cardJSON.cards[index].outsBalDrCrInd,     
                        cardJSON.cards[index].blockCode,          
                        cardJSON.cards[index].cardStatus,         
                        new Date(cardJSON.cards[index].cardExpiryDate),     
                        new Date(cardJSON.cards[index].cardIssueDate),      
                        cardJSON.cards[index].cardholderName,     
                        cardJSON.cards[index].creditLimit,        
                        cardJSON.cards[index].cardProduct        
                    );
                    objCountr ++;
                    cards.push(cardResp)
                } // EOF CONDITION                
            }// EOF LOOP
            if(objCountr > 0){
                let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
                metadata.status = "0000";
                metadata.description = "Success!";
                metadata.responseTime = new Date();
                resolve(new CardsResponse(metadata,cards).generateJSON());
            }else{
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Record not found"));         
            }
            
        });
    }
}