import {PostRespBusinessObjects} from "./Response";

interface ICard {
    pan:                string;
    cif:                string;
    accountNumber:      string;
    currency:           string;
    type:               string;
    isActivated:        string;
    isSupp:             string;
    availBal:           number;
    availBalDrCrInd:    string;
    outstandingBalance: number;
    outsBalDrCrInd:     string;
    blockCode:          number;
    cardStatus:         string;
    cardExpiryDate:     Date;
    cardIssueDate:      Date;
    cardholderName:     string;
    creditLimit:        number;
    cardProduct:        string;
}

export class CardBO implements ICard{

    pan:                string;
    cif:                string;
    accountNumber:      string;
    currency:           string;
    type:               string;
    isActivated:        string;
    isSupp:             string;
    availBal:           number;
    availBalDrCrInd:    string;
    outstandingBalance: number;
    outsBalDrCrInd:     string;
    blockCode:          number;
    cardStatus:         string;
    cardExpiryDate:     Date;
    cardIssueDate:      Date;
    cardholderName:     string;
    creditLimit:        number;
    cardProduct:        string;

    constructor( pan:                string,
                cif:                string,
                accountNumber:      string,
                currency:           string,
                type:               string,
                isActivated:        string,
                isSupp:             string,
                availBal:           number,
                availBalDrCrInd:    string,
                outstandingBalance: number,
                outsBalDrCrInd:     string,
                blockCode:          number,
                cardStatus:         string,
                cardExpiryDate:     Date,
                cardIssueDate:      Date,
                cardholderName:     string,
                creditLimit:        number,
                cardProduct:        string){
        this.pan                	=	pan                	;
        this.cif                	=	cif                	;
        this.accountNumber      	=	accountNumber      	;
        this.currency           	=	currency           	;
        this.type               	=	type               	;
        this.isActivated        	=	isActivated        	;
        this.isSupp             	=	isSupp             	;
        this.availBal           	=	availBal           	;
        this.availBalDrCrInd    	=	availBalDrCrInd    	;
        this.outstandingBalance 	=	outstandingBalance 	;
        this.outsBalDrCrInd     	=	outsBalDrCrInd     	;
        this.blockCode          	=	blockCode          	;
        this.cardStatus         	=	cardStatus         	;
        this.cardExpiryDate     	=	cardExpiryDate     	;
        this.cardIssueDate      	=	cardIssueDate      	;
        this.cardholderName     	=	cardholderName     	;
        this.creditLimit        	=	creditLimit        	;
        this.cardProduct        	=	cardProduct        	;                    

    }

}

export default class CardsResponse{
    private metadata!: PostRespBusinessObjects.Metadata;
    private cards!:CardBO[];

    constructor(metadata: PostRespBusinessObjects.Metadata, cards:CardBO[]){
        this.metadata = metadata;
        this.cards = cards;        
    }

    public generateJSON(): String{
        return JSON.stringify(this);
    }
}